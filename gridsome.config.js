// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
	siteName: "Docc",
	icon: {
		favicon: "./src/assets/favicon.png",
		touchicon: "./src/assets/favicon.png"
	},
	siteUrl:
		process.env.URL ||
		process.env.CI_PAGES_DOMAIN ||
		"http://gbitdev.gitlab.io/",
	pathPrefix: process.env.CI_COMMIT_REF_NAME
		? `/gridsome-cms/${
				process.env.CI_COMMIT_REF_NAME != "master"
					? process.env.CI_COMMIT_REF_NAME
					: ""
		  }`
		: "/",
	outputDir:
		process.env.CI_COMMIT_REF_NAME &&
		process.env.CI_COMMIT_REF_NAME !== "master"
			? `public/${process.env.CI_COMMIT_REF_NAME}`
			: "public",
	settings: {
		web: process.env.URL || false,
		twitter: process.env.URL_TWITTER || false,
		github: process.env.URL_GITHUB || false,
		nav: {
			links: [{ path: "/docs/", title: "Docs" }]
		},
		sidebar: [
			{
				name: "docs",
				sections: [
					{
						title: "Getting Started",
						items: [
							"/docs/",
							"/docs/installation/",
							"/docs/writing-content/",
							"/docs/deploying/"
						]
					},
					{
						title: "Configuration",
						items: ["/docs/settings/", "/docs/sidebar/"]
					}
				]
			}
		]
	},
	plugins: [
		{
			use: "@gridsome/source-filesystem",
			options: {
				baseDir: "./content",
				path: "**/*.md",
				typeName: "MarkdownPage",
				remark: {
					externalLinksTarget: "_blank",
					externalLinksRel: ["noopener", "noreferrer"],
					plugins: ["@gridsome/remark-prismjs"]
				}
			}
		},

		{
			use: "gridsome-plugin-netlify-cms",
			options: {
				enableIdentityWidget: false
			}
		},

		{
			use: "gridsome-plugin-tailwindcss",
			options: {
				tailwindConfig: "./tailwind.config.js",
				purgeConfig: {
					// Prevent purging of prism classes.
					whitelistPatternsChildren: [/token$/]
				}
			}
		},

		{
			use: "@gridsome/plugin-google-analytics",
			options: {
				id: process.env.GA_ID ? process.env.GA_ID : "XX-999999999-9"
			}
		},

		{
			use: "gridsome-plugin-manifest",
			options: {
				background_color: "#e2e8f0",
				icon_path: "./src/assets/favicon.png",
				name: "Gridsome PWA - Headless CMS",
				short_name: "Gridsome PWA",
				theme_color: "#5a67d8",
				lang: "pl"
			}
		},

		{
			use: "gridsome-plugin-brotli",
			options: {
				extensions: ["css", "html", "js", "svg", "json"]
			}
		},

		{
			use: "gridsome-plugin-service-worker",
			options: {
				cacheFirst: {
					routes: [
						"/",
						/\.(js|css|png)$/ // means "every JS, CSS, and PNG images"
					]
				}
			}
		},

		{
			use: "@gridsome/plugin-sitemap",
			options: {}
		}
	]
};
